##PRIMER CAPITULO: CONFIGURACIÓN

1. añadidos .eslintrc 

   reglas sintacticas https://github.com/airbnb/javascript airbnb
   
2. añadido .babelrc
   
   soporte es7 features (env),polyfill, regenerator
   
3. añadida configuración de módulos de css (webpack)
   
   icss-values, cssnext 

   OBJETIVOS 
   
   - integrar eslint https://eslint.org/docs/user-guide/integrations
   - arrancar el proyecto y comprobar que reciben un html con bienvenidos
   
##SEGUNDO - TERCER PASO: NUESTRO PRIMER COMPONENTE

1. creando nuestro primer componente de prueba

2. usando hot module replacement

3. poniendo estilos a nuestro primer componente

    OBJETIVOS 
    
    - conocer las ventajas de hmr 
    - como he podido vivir sin componentes funcionales y sin hmr
    
##CUARTO: COMO DAR ESTILOS A NUESTRO COMPONENTE

1. usando modulos de css, cssnext y icss-values

    OBJETIVOS
    
    - snapshot component programming
    - como estructurar nuestros estilos mejoras frente a BEM
    - paso de datos entre css y js (icss-values);

##QUINTO: ALIASES DE WEBPACK

1. aliases de webpack (the good the bad and the ugly)

    OBJETIVOS
    
    - aprender las bondades de los aliases (pero con cuidado)

##SEXTO: SERVER SIDE RENDERING

1. server side rendering 

    OBJETIVOS
    
    - que es?, que ventajas nos aporta.

##SEPTIMO: CONECTANDO CON EL CONTENEDOR DE ESTADOS

1. conectando a redux

2. acciones sincronas en cliente y servidor

3. acciones asíncronas en el cliente

4. acciones asíncronas en el servidor

    OBJETIVOS
   
    - la mentalidad necesaria a la hora de pensar en las acciones entre cliente y servidor.
    - abstracción total del motor de 'plantillas' que usemos (angular, vue,..).
    - la mejor manera de imaginar una aplicación con redux y componentes funcionales es:
      (https://es.wikipedia.org/wiki/Zo%C3%B3tropo)

##PREGUNTAS



##OBJETIVO FINAL

El objetivo principal del taller era explicar que el camino al isomorfismo puro, está plagado de sangre, sudor y lágrimas, sin embargo el stack indicado durante el taller (react, redux) y el hecho de ser unicamente componentes funcionales nos garantiza que podamos sortear algunos de los principales problemas.

De las muchas iteracciones (unas 5 entre borradores, ideas desechadas, ...) que se han llevado a cabo para elaborar el taller la mas organizada y facil de entender es la formada por los dos proyectos mencionados al final de esta sección.

En ambos proyectos están documentados todos los pasos con los principales problemas y como resolverlos.

Espero que disfruteis investigando y navegando.

- (APLICACIÓN)[https://bitbucket.org/corn_time_codemotion2017/corn_time_app]
- (COMPONENTES)[https://bitbucket.org/corn_time_codemotion2017/corn_time_components]

!!!NOTA

http://franciscomar.com.es/movies 

Se hará publica cuando sea estable y resuelva unas cuantas cuestiones :D 

mientras tanto feel free to inspire. Mantendré el slack habilitado para aquellos que querais

seguir investigando y haciendo preguntas sobre las cuestiones tratadas en el workshop.


##PASOS QUE TENEIS QUE AVERIGUAR VOSOTROS O ESPERAR AL PROXIMO AÑO ;)

- CREAR LOS BUNDLES DE PRODUCCIÓN 
- ISOMORFISMO CON ACCIONES ASÍNCRONAS EN EL SERVIDOR
- TECNICAS DE DEBUGGING Y LOGGING ISOMORFICAS :D
- CACHE BUSTING Y VERSIONING DE NUESTROS ASSETS CON WEBPACK
- GRABAR POSICION DEL SCROLL ENTRE PANTALLAS
- DIVIDIR LOS STORES POR PANTALLAS
- AUTOLOADING MODULES
- RESETEAR ESTILOS CON CSSMODULES